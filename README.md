# Blossom
After searching and being unable to find a CSS framework that used native imports, CSS variables that can be changed at runtime and other modern CSS techniques, I created Blossom.

## Features
- Minimal and small
- CSS variable driven
- Uses modern CSS, like :is for selector grouping and CSS imports
- No preprocessors used
- Reactive without breakpoints
- Dark mode built in
- Includes JS replacement functionality such as accordions using details and summary and tooltips using dataset attributes
- Mostly classless, leading to very clean markup
- Sensible defaults and CSS reset included

## Usage
Firstly, import Blossom.
This will vary based on where you're importing it but generally you should be able to just import blossom.css into your codebase.
This import can be handled either with a `link` tag or via a JS import depending on use case.
If preferred, you can run this through a pipeline to minify it or cull files that you aren't using.

To start with, wrap the page in a `main` element.
Most elements will be styled automatically with no classes added, but some classes are available (documented below).

```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>Hello world</title>
  </head>
  <body>
    <main>
      <div class="container">
        <h1>Hello world!</h1>
        <div class="card">
          <h2>A card</h2>
          <p>Lorem ipsum dolor sit amet</p>
          <button>Do something</button>
        </div>
      </div>
    </main>
  </body>
</html>
```

## Grid and page sections
To create a grid, make a div with class `grid` and place elements inside as direct children.

To add a width-constrained container to the page, add a div with class `container` to the page.
Containers are best used as direct children of the `main` element.

Most elements in Blossom have a default padding to separate them out.
If you are using an element that doesn't, adding the `block` class will add this padding.

## Elements
### Button
Buttons are automatically styled. You can mark a button as primary by adding class `primary` to it.
To style a non-button element as a button, give it class `button`.

### Sidebar
Aside elements are automatically styled as sidebars.
By default they're hidden, and when given the `open` class they show.
Place your aside element at the same level as the `main` element as a sibling.

### Card
Cards are denoted using a div with class of `card`. You can mark it as an alert or notice by adding class `alert` or `notice`

### Details accordion
`details` and `summary` elements are automatically styled.

### Forms
Forms are automatically styled, no need to add any classes.

### Modals
To create a modal, simply create a div with class `modal`. By default it'll be hidden, to show it add the class `open` to it.

### Navigation bar
`nav` elements should be placed as a sibling of your `main` element.

### Table
Tables are automatically styled. To make the header row sticky (scroll with the browser window), add class `sticky` to the table.
Tables require use of `thead`, `tbody` and `th` for full functionality.

### Tooltip
Tooltips can be added to any element by adding an `data-tooltip="Example tooltip"` attribute to it.
